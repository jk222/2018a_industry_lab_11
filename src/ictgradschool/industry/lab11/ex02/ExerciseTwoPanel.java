package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{

    private JTextField firstMathValue;
    private JTextField secondMathValue;

    private JButton calculateAdditionButton;
    private JButton calculateSubtractButton;

    private JTextField resultingValue;


    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        firstMathValue = new JTextField(10);
        secondMathValue = new JTextField(10);


        calculateAdditionButton = new JButton("Addition");
        calculateSubtractButton = new JButton("Subtract");

        resultingValue = new JTextField(20);

        JLabel resultLabel = new JLabel("Result: ");

        this.add(firstMathValue);
        this.add(secondMathValue);
        this.add(calculateAdditionButton);
        this.add(calculateSubtractButton);
        this.add(resultLabel);
        this.add(resultingValue);

        calculateAdditionButton.addActionListener(this);
        calculateSubtractButton.addActionListener(this);

    }

    public void actionPerformed(ActionEvent event) {


        if (event.getSource() == calculateAdditionButton){


            double firstValue = Double.parseDouble(firstMathValue.getText());
            double secondValue = Double.parseDouble(secondMathValue.getText());


            double additionCalculatedValue = roundTo2DecimalPlaces(firstValue + secondValue);

            String stringResult = String.valueOf(additionCalculatedValue);
            resultingValue.setText(stringResult);
        }

        else if(event.getSource() == calculateSubtractButton){

            double firstValue = Double.parseDouble(firstMathValue.getText());
            double secondValue = Double.parseDouble(secondMathValue.getText());

            double subtractionCalculatedValue = roundTo2DecimalPlaces(firstValue - secondValue);

            String stringResult = String.valueOf(subtractionCalculatedValue);
            resultingValue.setText(stringResult);
        }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}
