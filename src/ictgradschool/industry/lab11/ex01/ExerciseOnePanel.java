package ictgradschool.industry.lab11.ex01;

import javax.imageio.plugins.bmp.BMPImageWriteParam;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {
    // TODO Declare JTextFields and JButtons as instance variables here.
    private JTextField heightInMetres;
    private JTextField weightInKilograms;
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeight;
    private JTextField bmiValue;
    private JTextField maximumHealthyWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.LIGHT_GRAY);
        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        heightInMetres = new JTextField(10);




        weightInKilograms = new JTextField(10);
        bmiValue = new JTextField(10);
        maximumHealthyWeight = new JTextField(10);
        calculateBMIButton = new JButton("Calculate BMI");


        calculateHealthyWeight = new JButton("Calculate Healthy Weight");


        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightInMetresLabel = new JLabel("Height in metres: ");


        JLabel weightInKilogramsLabel = new JLabel("Weight in kilograms: ");
        JLabel bmiValueLabel = new JLabel("Your Body Mass Index (BMI) is: ");
        JLabel maximumHealthyWeightLabel = new JLabel("Maximum Healthy Weight for your Height : ");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightInMetresLabel);
        this.add(heightInMetres);
        this.add(weightInKilogramsLabel);
        this.add(weightInKilograms);
        this.add(calculateBMIButton);
        this.add(bmiValueLabel);
        this.add(bmiValue);
        this.add(calculateHealthyWeight);
        this.add(maximumHealthyWeightLabel);
        this.add(maximumHealthyWeight);

        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthyWeight.addActionListener(this);
    }

    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {
        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        if (event.getSource() == calculateBMIButton) {
            double heightMeasure = Double.parseDouble(heightInMetres.getText());
            double weightMeasure = Double.parseDouble(weightInKilograms.getText());
            double bmiCalculationValue = roundTo2DecimalPlaces(weightMeasure / (heightMeasure * heightMeasure));
            String stringBMI = String.valueOf(bmiCalculationValue);
            bmiValue.setText(stringBMI);

        } else if (event.getSource() == calculateHealthyWeight) {
            double heightMeasure = Double.parseDouble(heightInMetres.getText());
            double healthyWeightMeasure = roundTo2DecimalPlaces(24.9 * heightMeasure * heightMeasure);
            String stringHealthyWeight = String.valueOf(healthyWeightMeasure);
            maximumHealthyWeight.setText(stringHealthyWeight);
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     *
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }
}
