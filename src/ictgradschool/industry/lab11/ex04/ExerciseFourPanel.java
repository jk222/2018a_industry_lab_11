package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private Timer timer;
    private java.util.List<Balloon> balloonArray;


    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloonArray = new ArrayList<Balloon>(50);

        balloonArray.add(new Balloon(30, 60));


        for (int i = 0; i < 10; i++) {
            int randomNumber = (int) (Math.random() * 10) +1;
            balloonArray.add(new Balloon(randomNumber, randomNumber));
        }


        addKeyListener(this);

        this.timer = new Timer(300, this);
        this.timer.start();
    }

    /* Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
    *
    * @param e
    */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == timer) {
            for (int i = 0; i < balloonArray.size(); i++) {
                balloonArray.get(i).move();
            }
        }

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();
        repaint();
    }

    /* Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (int i = 0; i < balloonArray.size(); i++) {
            balloonArray.get(i).draw(g);
        }

        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {

        for (int i = 0; i < balloonArray.size(); i++) {

            if (e.getKeyCode() == KeyEvent.VK_UP) {
                balloonArray.get(i).setDirection(Direction.Up);
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                balloonArray.get(i).setDirection(Direction.Down);
            }
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                balloonArray.get(i).setDirection(Direction.Left);
            }
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                balloonArray.get(i).setDirection(Direction.Right);
            }
            if (e.getKeyCode() == KeyEvent.VK_S) {
                balloonArray.get(i).setDirection(Direction.None);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
